import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class ServicesService {
  BaseUrl: String = 'http://localhost:1337/api/';
  router: any;
  constructor(private http: HttpClient) { }

  postProduct(data: any) {
    return this.http.post<any>("http://localhost:1337/api/product", data)
      .pipe(map((res: any) => {
        return res;
      }))
  }

  ListProduct() {
    let a = this.http.get(this.BaseUrl + 'products')
    console.log(a)
    return a
  }

  SearchProduct(product: any) {
    console.log("service Search : ", product)
    return this.http.get(this.BaseUrl + 'products/' + product)
      .pipe(map((res: any) => {
        console.log(res)
        return res;
      }))
  }


  DeleteProduct(product: any) {
    console.log("service delete : ", product)
    return this.http.delete(this.BaseUrl + 'products/' + product)
      .pipe(map((res: any) => {
        console.log(res)
        return res;
      }))
  }

  DeleteProduct2(product: any) {
    console.log("service delete : ", product)
    let b = this.http.delete(this.BaseUrl + 'products/' + product)
    console.log(b)
    return b
  }

  UpdateProduct(id: any, product: any) {
    console.log("service update : ", id, "With ", product)
    let uri = "http://localhost:1337/api/products/" + id
    console.warn(uri)

    this.http.put<any>("http://localhost:1337/api/products/", id, product)
      .pipe(map((res: any) => {
        console.log(res)

        return res;
      }))


  }


  UpdateProudct2(id: any, product: any) {

    this.http.put<any>('http://localhost:1337/api/products/'+ id, product)
      .subscribe(data => console.log(data));

  }

}







