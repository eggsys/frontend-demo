import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductFormComponent } from './components/product-form/product-form.component';
import { ProductListComponent } from './components/product-list/product-list.component';
import { ProductUpdateComponent } from './components/product-update/product-update.component';

const routes: Routes = [
  {path:'List', component: ProductListComponent},
  {path:'Form-Add', component: ProductFormComponent},
  {path:'product/:id', component: ProductUpdateComponent},
  {path:'update/:id' , component: ProductUpdateComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
