import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ServicesService } from 'src/app/services/services.service';


@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  listproduct: any
  formValue !: FormGroup
  
  constructor(private ProductService: ServicesService, private formbuilder: FormBuilder) { }

  ngOnInit(): void {
    this.formValue = this.formbuilder.group({
      title: [''],
      price: [''],
      quantity: [''],
    })
    this.getAllProduct()
  }

  getAllProduct() {
    this.ProductService.ListProduct()
      .subscribe(res => {
        this.listproduct = res;
      })
  }

  deleteProduct(product: any) {
    console.log("call : DELETE")
    this.ProductService.DeleteProduct(product._id)
      .subscribe(res => {
        alert("Product Delete" + res);
        this.getAllProduct();
      })
  }
/*
  UpdateProduct(product : any ){
    console.log("call : Update ")
    this.ProductService.UpdateProduct(product._id)
    //FormComponent.insertForm.control['title'].setValue(product.ProductTitle)

  }*/

}
