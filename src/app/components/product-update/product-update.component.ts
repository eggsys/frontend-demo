import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormModel } from '../product-form/product-form.model';
import { ServicesService } from 'src/app/services/services.service';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";


@Component({
  selector: 'app-product-update',
  templateUrl: './product-update.component.html',
  styleUrls: ['./product-update.component.scss']
})


export class ProductUpdateComponent implements OnInit {
  insertForm: FormGroup
  productModelObj: FormModel = new FormModel()

  constructor(private router: Router,
    private route: ActivatedRoute,
    private ProductService: ServicesService,
    private formbuilder: FormBuilder,
    private api: ServicesService

  ) {
    // route.data includes both `data` and `resolve`
    this.insertForm = this.formbuilder.group({
      title: formbuilder.control('', [Validators.required]),
      price: formbuilder.control('', [Validators.min(0)]),
      quantity: formbuilder.control('', [Validators.min(0)]),


    })

  }

  public onSubmit() {
    console.log(this.insertForm.value)
  }

  public control(name: string) {
    return this.insertForm.get(name)
  }



  product_detail !: any
  user !: { id: string }
  ngOnInit(): void {
    console.log("READY")
    /*
        this.user = {
          id :this.route.snapshot.params['id']
        }
    
        */
    this.route.params.subscribe((data: Params) => {
      this.user = {
        id: data['id']
      }
    })

    console.log(this.user)

    this.SearchProduct(this.user.id)


  }

  SearchProduct(product: any) {
    console.log("call : SEARCH", product)

    this.ProductService.SearchProduct(product)
      .subscribe(res => {
        alert("Product Found" + res);
        this.product_detail = res
        this.insertForm = this.formbuilder.group({
          title: this.formbuilder.control(res['ProductTitle'], [Validators.required]),
          price: this.formbuilder.control(res['ProductPrice'], [Validators.min(0)]),
          quantity: this.formbuilder.control(res['ProductQuantity'], [Validators.min(0)]),


        })

      })
  }
  UpdateProductDetail() {

    this.productModelObj.ProductTitle = this.insertForm.value.title
    this.productModelObj.ProductPrice = this.insertForm.value.price
    this.productModelObj.ProductQuantity = this.insertForm.value.quantity
    console.log("Object ==>", this.productModelObj)
    this.api.UpdateProudct2(this.user.id, this.productModelObj)
    this.router.navigate(['/List'])
    

    alert("MOCKUP POST" + this.productModelObj.ProductTitle)


  }



}


